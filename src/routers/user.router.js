import { Router } from 'express';
import { getUsers, createUser, updateUser, deleteUser } from './../controllers/user.controller';

const router = new Router();

//http://localhost:4000/api/users
router.get('/',getUsers);

//http://localhost:4000/api/users
router.post('/',createUser);

//http://localhost:4000/api/users/idUser
router.put('/:id',updateUser);

//http://localhost:4000/api/users/idUser
router.delete('/:id',deleteUser);


export default router;