import app from './../server';

//middlewares
import { isAuth } from './../middleware/isauth.middleware';

import userRouters from './../routers/user.router';
import authRouter from './../routers/auth.router';

app.use('/api/users', isAuth,userRouters);
app.use('/api/auth', authRouter);
