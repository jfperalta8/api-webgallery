import app from './server';
import config from './config/config';

import './database';
import './routers/index';


async function main (){
    await app.listen(config.port);
    console.log(`server on port ${config.port}`);
}

main();