import { Schema, model } from 'mongoose';

const AddressUser = new Schema({
    user_id: {
        type:String,
        required: true
    },
    direction: {
        type:String,
        required: true
    },
    reference_direction: {
        type:String,
        required: true
    }
});

export default model('user_address',AddressUser);