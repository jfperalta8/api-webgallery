import { Schema, model } from 'mongoose';

const DataPersonal = new Schema({

    user_id:{
        type: String,
        required : true
    },
    name: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    number_document: {
        type: String,
        required: true
    },
    type_document_id:{
        type: String,
        required: true
    },
    phone:{
        type: Number,
        required: true
    },
    time_create:{
        type: Date,
        required: true,
        default: new Date()
    },
    time_update:{
        type: Date,
        required: true,
        default: new Date()
    }

});

export default model('data_personal',DataPersonal);