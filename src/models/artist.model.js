import { Schema, model } from 'mongoose';

const ArtistSchema = new Schema({
    user_id: {
        type:String,
        required: true
    },
    biagraphy: {
        type:String,
        default: "Sin biografia"
    }
});


export default model('artits', ArtistSchema);