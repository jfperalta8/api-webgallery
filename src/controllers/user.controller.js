import User from './../models/user.model';

async function getUsers(req, res) {
    try {
        const users = await User.find();
        return res.json({
            data: users
        });
    } catch (error) {
        res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor" });
    }
}

async function createUser(req, res) {
    try {
        let user = new User({
            ...req.body
        })
        user.password = user.encryptPassword(user.password);
        user.time_create = new Date();
        user.time_update = new Date();
        await user.save();

        res.json({ message: 'El usuario a sido creado con exito!!', data: user })
    } catch (error) {
        res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error });
    }

}

async function updateUser(req, res){
    try {
        let dateUpdate = req.body;
        const { id } = req.params;
        let user = new User();

        dateUpdate.time_update = new Date();
        if(dateUpdate.password != null){
            dateUpdate.password = user.encryptPassword(dateUpdate.password)
        }
        await User.updateOne({_id: id},{...dateUpdate});

        res.json({ message: 'El usuario a sido editado con exito!!', data: dateUpdate })
    } catch (error) {
        res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error });
    }
}

async function deleteUser(req, res){

    try {
        const { id } = req.params;
        const userDelete = await User.deleteOne({_id: id});
        res.json({ message: 'El usuario a sido eliminado con exito!!', data: userDelete })
    } catch (error) {
        res.status(500).json({ success: false, message: "Ha ocurrido un problema en el servidor", error });
    }

}


module.exports = {
    getUsers,
    createUser,
    updateUser,
    deleteUser
}