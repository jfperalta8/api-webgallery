module.exports = {
    port: process.env.PORT || 4000,
    db: process.env.db || 'mongodb://localhost:27017/webgalley',
    secretKeyToken: 'MyS3cR3tK3yJs0W3bT0k3N'
}