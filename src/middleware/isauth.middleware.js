import jwt from 'jsonwebtoken';
import { secretKeyToken } from './../config/config';
function isAuth(req, res, next) {
    try {
        if (req.get('Authorization') != null) {
            let token = req.get('Authorization').split(" ").pop();
            console.log(token)
            let decode = jwt.verify(token, secretKeyToken);
            if (decode == null) {
                res.status(401).json({ succes: false, message: "Token invalido" });
            }
            next();
        }else{
            res.status(401).json({ succes: false, message: "suministra un token valido para poder acceder a esta ruta" });
        }


    } catch (error) {
        return res.status(500).json({ success: false, message: "Ha ocurrido un problema validando el token", error })
    }
}

module.exports = {
    isAuth
}